/**
* @package Helix3 Framework
* @author JoomShaper http://www.joomshaper.com
* @copyright Copyright (c) 2010 - 2015 JoomShaper
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
jQuery(function($) {
  var $body = $('body'),
  $wrapper = $('.body-innerwrapper');
    $('#offcanvas-toggler').on('click', function(event){
        event.preventDefault();
        $('body').addClass('offcanvas');
    });

    $( '<div class="offcanvas-overlay"></div>' ).insertBefore( '.body-innerwrapper > .offcanvas-menu' );

    $('.close-offcanvas, .offcanvas-overlay').on('click', function(event){
        event.preventDefault();
        $('body').removeClass('offcanvas');
    });

  //Mega Menu
  $('.sp-megamenu-wrapper').parent().parent().css('position','static').parent().css('position', 'relative');
  $('.sp-menu-full').each(function(){
    $(this).parent().addClass('menu-justify');
  });

  //Sticky Menu
  $(document).ready(function(){
    $("body.sticky-header").find('#sp-navigation').sticky({topSpacing:0})
  });
  var consCarousel = function(elm) {
		$(elm).each(function() {
			if ( $().owlCarousel ) {
				$(this).owlCarousel({
					items: $(this).data('items'),
					itemsDesktop: [1199,$(this).data('itemsdesktop')],
					itemsDesktopSmall:[979,$(this).data('itemsdesktopsmall')],
					itemsTablet: [768,$(this).data('itemstablet')],
					itemsMobile: [479,$(this).data('itemsmobile')],
					slideSpeed: $(this).data('slidespeed'),
					autoPlay: $(this).data('autoplay'),
					pagination: $(this).data('pagination'),
					responsive: $(this).data('responsive')
				});
			}
		});
	};
  consCarousel('.testimonial');
  consCarousel('.testimonial03');
  var goTop = function() {
  $('.totop a').on('click', function() {
    $("html, body").animate({ scrollTop: 0 }, 1000 , 'easeInOutExpo');
    return false;
  });
};
goTop();

var popupGallery = function() {
  if( $().magnificPopup ) {
    $('.popup-gallery').magnificPopup({
      delegate: 'a.popup',
      type: 'image',
      removalDelay: 600,
      tLoading: 'Loading image #%curr%...',
      mainClass: 'my-mfp-slide-bottom',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function(item) {
          return item.el.attr('title');
        }
      }
    });
  }
};

popupGallery();
  //Tooltip
  $('[data-toggle="tooltip"]').tooltip();

  $(document).on('click', '.sp-rating .star', function(event) {
    event.preventDefault();

    var data = {
      'action':'voting',
      'user_rating' : $(this).data('number'),
      'id' : $(this).closest('.post_rating').attr('id')
    };

    var request = {
      'option' : 'com_ajax',
      'plugin' : 'helix3',
      'data'   : data,
      'format' : 'json'
    };

    $.ajax({
      type   : 'POST',
      data   : request,
      beforeSend: function(){
        $('.post_rating .ajax-loader').show();
      },
      success: function (response) {
        var data = $.parseJSON(response.data);

        $('.post_rating .ajax-loader').hide();

        if (data.status == 'invalid') {
          $('.post_rating .voting-result').text('You have already rated this entry!').fadeIn('fast');
        }else if(data.status == 'false'){
          $('.post_rating .voting-result').text('Somethings wrong here, try again!').fadeIn('fast');
        }else if(data.status == 'true'){
          var rate = data.action;
          $('.voting-symbol').find('.star').each(function(i) {
            if (i < rate) {
              $( ".star" ).eq( -(i+1) ).addClass('active');
            }
          });

          $('.post_rating .voting-result').text('Thank You!').fadeIn('fast');
        }

      },
      error: function(){
        $('.post_rating .ajax-loader').hide();
        $('.post_rating .voting-result').text('Failed to rate, try again!').fadeIn('fast');
      }
    });
  });

});
